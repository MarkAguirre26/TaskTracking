package com.hsgutilization.android.hsgutilizationtracking;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.json.JsonConverter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Home_dashboard extends Activity {

    final String TAG = this.getClass().getSimpleName();
    ListView listView;
    TextView emp_name,txt_ID,txt_user_cn;
    String TaskID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_dashboard);

        Bundle extras = getIntent().getExtras();
        String emp = extras.getString("Name");

        txt_user_cn = (TextView)findViewById(R.id.txt_user_cn);
        listView =  (ListView)findViewById(R.id.listTaskView);
        emp_name = (TextView)findViewById(R.id.emp_name);

        emp_name.setText(emp);

        txt_user_cn.setText(extras.getString("usercn"));
        txt_user_cn.setVisibility(View.GONE);

      listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


              String t_ID = ((TextView) view.findViewById(R.id.txt_Taskcn)).getText().toString();
              Intent intent = new Intent(getApplicationContext(), TimeTrack.class);
              intent.putExtra("TASK_ID", t_ID);
              startActivity(intent);


          }
      });





          StringRequest stringRequest = new StringRequest(Request.Method.POST, url.url_taskList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("Mesage", response);

                        String[] splitted = response.split("/");

                       if(splitted.length >1) {
                        Toast.makeText(getApplicationContext(),"No Data ",Toast.LENGTH_LONG).show();
                       }
                        else {

                        ArrayList<TaskList_model> jsonObjects = new JsonConverter<TaskList_model>().toArrayList(response, TaskList_model.class);

                        BindDictionary<TaskList_model> dictionary = new BindDictionary<>();
                        dictionary.addStringField(R.id.txt_Taskcn, new StringExtractor<TaskList_model>() {
                            @Override
                            public String getStringValue(TaskList_model item, int position) {
                                return item.task_cn;
                            }
                        });
                        dictionary.addStringField(R.id.txt_Tasktitle, new StringExtractor<TaskList_model>() {
                            @Override
                            public String getStringValue(TaskList_model item, int position) {
                                return item.task_title;
                            }
                        });

                        dictionary.addStringField(R.id.txt_description, new StringExtractor<TaskList_model>() {
                            @Override
                            public String getStringValue(TaskList_model item, int position) {
                                return item.description;
                            }
                        });
                        FunDapter<TaskList_model> adapter = new FunDapter<>(getApplicationContext(),jsonObjects,R.layout.task_listview_row,dictionary);
                        listView.setAdapter(adapter);
                        }


                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error while reading data", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("User_cn", txt_user_cn.getText().toString());

                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);


    }





    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        Home_dashboard.this.finish();
                    }
                }).create().show();
            }


}
