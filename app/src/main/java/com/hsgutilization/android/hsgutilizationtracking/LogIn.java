package com.hsgutilization.android.hsgutilizationtracking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class LogIn extends Activity {

    final String TAG = "result";
    EditText txt_username, txt_password;
LinearLayout layout_NoNetwork;
    Button btn_login;
String uname,Pword;
    android.os.Handler handler = new android.os.Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_log_in);

        // Forget Password section
        TextView buttonCreateLocation = (TextView) findViewById(R.id.linkForgetPass);
      buttonCreateLocation.setOnClickListener(new OnClickListenerChangePass());
        layout_NoNetwork = (LinearLayout)findViewById(R.id.layout_NoNetwork);
        txt_username = (EditText)findViewById(R.id.Txt_EmpNum);
        txt_password = (EditText)findViewById(R.id.Txt_password);
        btn_login = (Button)findViewById(R.id.btn_login);
        layout_NoNetwork.setVisibility(View.GONE);

        txt_username.setText("1");
        txt_password.setText("1");

    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




        //Login section
  public   void cmd_login (View view) {

      if(!isNetworkAvailable()){
          Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
      }
      else
      {
                  uname = txt_username.getText().toString();
                  Pword = txt_password.getText().toString();
                  if (uname.isEmpty() || Pword.isEmpty()) {
                      Toast.makeText(getApplication(), "Invalid Input", Toast.LENGTH_SHORT).show();
                      return;
                  } else {

                      StringRequest stringRequest = new StringRequest(Request.Method.POST, url.url_login,
                     // StringRequest stringRequest = new StringRequest(Request.Method.POST, variable.url_login,
                              new Response.Listener<String>() {
                                  @Override
                                  public void onResponse(String response) {
                                      Log.d(TAG, response);
                                      if (response.contains("wala")) {

                                          Toast.makeText(getApplicationContext(), "Wrong Username or Password", Toast.LENGTH_SHORT).show();

                                      } else {


                                           LogIn.this.finish();
                                            Intent intent = new Intent(getApplicationContext(), Home_dashboard.class);
                                            intent.putExtra("usercn",  uname);
                                            intent.putExtra("Name", response);
                                           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);

                                      }

                                    //  }
                                  }
                              }, new Response.ErrorListener() {
                          @Override
                          public void onErrorResponse(VolleyError error) {
                              Toast.makeText(getApplicationContext(), "Error while reading data "+error.getMessage(), Toast.LENGTH_SHORT).show();
                          }
                      }) {
                          @Override
                          protected Map<String, String> getParams() throws AuthFailureError {
                              Map<String, String> params = new HashMap<>();
                              params.put("uname", txt_username.getText().toString());
                              params.put("pwd", txt_password.getText().toString());
                              return params;
                          }
                      };
                      MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
                 }

      }

  }
}
