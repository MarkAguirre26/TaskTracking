package com.hsgutilization.android.hsgutilizationtracking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aaron on 7/20/2016.
 */
public class TaskList_model {

    @SerializedName("task_cn")
    public String task_cn;
    @SerializedName("task_title")
    public  String task_title;
    @SerializedName("description")
    public  String description;
}
