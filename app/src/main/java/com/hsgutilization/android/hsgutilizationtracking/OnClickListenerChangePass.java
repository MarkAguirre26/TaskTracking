package com.hsgutilization.android.hsgutilizationtracking;

/**
 * Created by johngo on 26/07/16.
 */
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class OnClickListenerChangePass implements View.OnClickListener {
    @Override
    public void onClick(View view) {

        final Context context = view.getContext();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.input_change_password_form, null, false);

        final EditText editEmail = (EditText) formElementsView.findViewById(R.id.editEmail);
        final EditText editEmpID = (EditText) formElementsView.findViewById(R.id.editEmpID);


        new AlertDialog.Builder(context)
                .setView(formElementsView)
                .setTitle("Forgot Password?")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                // dialog.cancel();

                                String method = "forgotpassword";
                                String employeeID =  editEmpID.getText().toString();
                                String email =  editEmail.getText().toString();

                                ForgotPassword forgotPassword = new ForgotPassword(context);
                                forgotPassword.execute(method,employeeID,email);
                            }

                        }).show();



    }
}