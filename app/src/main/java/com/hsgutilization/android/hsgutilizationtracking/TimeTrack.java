package com.hsgutilization.android.hsgutilizationtracking;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.ActivityChooserView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.kosalgeek.android.json.JsonConverter;
import com.kosalgeek.android.photoutil.GalleryMultiPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.PhotoLoader;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class TimeTrack extends Activity {

    private MediaPlayer mediaPlayer;
    ImageButton cmd_process;
    TextView txt_taskName, txt_timeTrack, txt_TimeStart, txt_TimeEnd, txt_taskDescription;
    public int ms, s, m, h;
    int TAG = 0;
    double latitude;
    double longitude;
    boolean isActiveTrack;
    String Address;
    private RequestQueue requestQueue;

    //Context baseContext;
    GPSTracker gps;
    Handler handler = new Handler();
    String Task_ID, Emp_cn;
    private boolean isUpdate;

    GalleryPhoto galleryPhoto;
    final int GALLERY_REQUEST = 1200;
    final String Tag_ = this.getClass().getSimpleName();

    String photoPath;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_time_track);

        // ImageButton buttonCreateLocation = (ImageButton) findViewById(R.id.btnAddNote);
        // buttonCreateLocation.setOnClickListener(new OnClickListenerAddNote());
        // baseContext = getBaseContext();
        requestQueue = Volley.newRequestQueue(this);
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        Intent intent = getIntent();
        String value = intent.getStringExtra("TASK_ID");
        String[] splited = value.split("-");
        Task_ID = splited[0];
        Emp_cn = splited[1];

        //   Toast.makeText(getApplicationContext(),"Activity Started",Toast.LENGTH_SHORT).show();
        mediaPlayer = MediaPlayer.create(this, R.raw.notif);
        txt_TimeStart = (TextView) findViewById(R.id.task_start);
        txt_TimeEnd = (TextView) findViewById(R.id.task_end);
        txt_taskName = (TextView) findViewById(R.id.task_name);
        txt_timeTrack = (TextView) findViewById(R.id.txt_timeTrack);
        cmd_process = (ImageButton) findViewById(R.id.cmd_process);
        txt_taskDescription = (TextView) findViewById(R.id.txt_taskDescription);

        getRenderTime();


    }


    Runnable UpdateTime = new Runnable() {
        @Override
        public void run() {
            ms = ms + 1;
            if (ms >= 59) {
                ms = 0;
                s = s + 1;
                if (s >= 59) {
                    s = 0;
                    m = m + 1;
                    if (m >= 59) {
                        m = 0;
                        h = h + 1;
                    }
                }
            }
            txt_timeTrack.setText(String.format("%02d", h) + ":" + String.format("%02d", m) + ":" + String.format("%02d", s));


            handler.postDelayed(this, 0);
        }
    };

private  void getRenderTime(){


    StringRequest stringRequest = new StringRequest(Request.Method.POST, url.url_renderTime,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Mesage", response);
                    String CurrentString = response;
                            String[] separated = CurrentString.split("-");
                            TAG = 0;

                            if (response.contains("wala")) {
                                ms = 2;
                                s = 0;
                                m = 0;
                                h = 0;

                                //  Toast.makeText(getApplicationContext(),ex.getMessage().toString(),Toast.LENGTH_LONG).show();
                            } else {
                                txt_timeTrack.setTextColor(Color.BLUE);
                                txt_timeTrack.setText(separated[0].toString());
                                txt_taskName.setText(separated[1].toString());
                                txt_taskDescription.setText(separated[2].toString() + "\n" + separated[3].toString());
                                String rendered_time = separated[0].toString();
                                String[] splited_value = rendered_time.split(":");
                                try {
                                    s = Integer.parseInt(splited_value[2]);
                                    m = Integer.parseInt(splited_value[1]);
                                    h = Integer.parseInt(splited_value[0]);
                                } catch (Exception ex) {

                                   Log.d("TAG",ex.getMessage());
                                  //  Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
                                }


                            }


                }
            },  new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getApplicationContext(), "Error while reading data", Toast.LENGTH_SHORT).show();
        }
    }) {
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> params = new HashMap<>();
            params.put("Task_cn", Task_ID);
            params.put("Emp_cn", Emp_cn);
            return params;
        }
    };
    MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

}
    public void btnUploadFile_Clicked(View view) {


        Intent intent = galleryPhoto.openGalleryIntent();
        startActivityForResult(intent, GALLERY_REQUEST);


    }
    //////////////////

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final MyCommand myCommand = new MyCommand(getApplicationContext());


        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                galleryPhoto.setPhotoUri(data.getData());
                photoPath = galleryPhoto.getPath();
                //imageList.add(photoPath);

                Log.d(Tag_, photoPath);


                try {

                    Bitmap bitmap = PhotoLoader.init().from(photoPath).requestSize(512, 512).getBitmap();

                    ImageView img = new ImageView(getApplicationContext());
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    img.setScaleType(ImageView.ScaleType.FIT_XY);
                    img.setLayoutParams(lp);
                    img.setImageBitmap(bitmap);

                    new AlertDialog.Builder(this)
                            .setView(img)
                            // .setTitle("Add Note")
                            .setCancelable(true)
                            .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Statement

                                    //"InsertTrack("http://192.168.2.121/WebDev/InserFile.php", "FIle Path", "-", null,");

                                }
                            })
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    try {
                                        Bitmap bitmap = PhotoLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
                                        final String encodedString = ImageBase64.encode(bitmap);

                                        //  String url = "http://192.168.2.121/WebDev/uploadImage.php";
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, variable.url_Upload, new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                                            }
                                        }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Toast.makeText(getApplicationContext(), "Error while uploading image", Toast.LENGTH_SHORT).show();
                                            }
                                        }) {
                                            @Override
                                            protected Map<String, String> getParams() throws AuthFailureError {
                                                Map<String, String> params = new HashMap<>();
                                                params.put("image", encodedString);
                                                params.put("ID1", Emp_cn);
                                                params.put("ID2", Task_ID);
                                                params.put("ID3", photoPath.substring(photoPath.lastIndexOf("/") + 1));
                                                return params;
                                            }
                                        };

                                        myCommand.add(stringRequest);

                                    } catch (FileNotFoundException e) {
                                        Toast.makeText(getApplicationContext(), "Error while loading image", Toast.LENGTH_SHORT).show();
                                    }
                                    //}


                                    myCommand.execute();

                                }
                            }).show();

                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Error while loading image", Toast.LENGTH_SHORT).show();
                }


            }
        }
        //super.onActivityResult(requestCode, resultCode, data);


    }


    public boolean getMyCurrentLocation() {

        boolean istrue;
        gps = new GPSTracker(TimeTrack.this);

        if (gps.canGetLocation()) {

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            istrue = true;
        } else {

            // gps.showSettingsAlert();
            istrue = false;
        }

        return istrue;

    }

    public void LatLongToAddress() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Address = response.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue.add(jsonObjectRequest);
    }


    public void cmd_process_Clicked(View view) {


        if (getMyCurrentLocation()) {

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            LatLongToAddress();

            if (TAG == 0) {
                TAG = 1;
                txt_timeTrack.setTextColor(Color.RED);
                handler.postDelayed(UpdateTime, 0);
                Drawable drawable = getResources().getDrawable(R.drawable.stop_time_track);
                cmd_process.setBackground(drawable);
                txt_TimeStart.setText(DateFormat.getDateTimeInstance().format(new Date()));
                txt_TimeEnd.setText("00:00:00");
                InsertTrack(variable.url_insert, "Start Track", txt_timeTrack.getText().toString(), txt_TimeStart.getText().toString(), "00:00:00");

                ScheduleUpdate(true);
            } else {

                TAG = 0;
                txt_timeTrack.setTextColor(Color.BLUE);
                handler.removeCallbacks(UpdateTime);
                Drawable drawable = getResources().getDrawable(R.drawable.start_time_track);
                cmd_process.setBackground(drawable);
                txt_TimeEnd.setText(DateFormat.getDateTimeInstance().format(new Date()));
                InsertTrack(variable.url_insert, "Stop Track", txt_timeTrack.getText().toString(), txt_TimeStart.getText().toString(), txt_TimeEnd.getText().toString());
                ScheduleUpdate(false);
            }
        } else {
            gps.showSettingsAlert();
        }


    }


    public void InsertTrack(final String url, final String Note, final String renderedTime, final String Date_Start, final String Date_End) {

        if (Note.contains("Interval Update")) {
            mediaPlayer.start();
        } else if (Note.contains("Stop Track")) {
            mediaPlayer.pause();
            mediaPlayer.stop();
        }


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Statement
                        //  Toast.makeText(baseContext, "Insert", Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(getApplicationContext(), "Error while reading data", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("ID1", Task_ID);
                params.put("ID2", Emp_cn);
                params.put("ID3", Note);
                params.put("ID4", renderedTime);
                params.put("ID5", Date_Start);
                params.put("ID6", Date_End);
                params.put("ID7", latitude + "," + longitude);
                params.put("ID8", Address);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

    private void ScheduleUpdate(final Boolean isActivate) {


        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (isActivate) {


                    InsertTrack(variable.url_insert, "Interval Update", txt_timeTrack.getText().toString(), txt_TimeStart.getText().toString(), txt_TimeEnd.getText().toString());


                }
            }
        }, 30000, 30000);
    }

    @Override
    public void onBackPressed() {
        if (TAG == 1) {
            Toast.makeText(getApplicationContext(), "Time Track is ongoing!", Toast.LENGTH_SHORT).show();
        } else {
            mediaPlayer.pause();
            mediaPlayer.stop();
            this.finish();
        }

    }


    public void cmd_addNote_Clicked(View view) {

        final EditText input = new EditText(TimeTrack.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        new AlertDialog.Builder(this)
                .setView(input)
                .setTitle("Add Note")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InsertTrack(variable.url_insert, input.getText().toString(), txt_timeTrack.getText().toString(), txt_TimeStart.getText().toString(), txt_TimeEnd.getText().toString());

                    }
                }).show();


    }


}
