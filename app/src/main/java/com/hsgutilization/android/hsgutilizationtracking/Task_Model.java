package com.hsgutilization.android.hsgutilizationtracking;

import com.google.gson.annotations.SerializedName;

/**
 * Created by johngo on 24/07/16.
 */
public class Task_Model {


    @SerializedName("task_title")
    public  String task_title;
    @SerializedName("time_rendered")
    public  String time_rendered;

}
