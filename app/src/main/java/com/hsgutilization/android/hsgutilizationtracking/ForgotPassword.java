package com.hsgutilization.android.hsgutilizationtracking;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Aaron on 7/17/2016.
 */
public class ForgotPassword extends AsyncTask<String,Void,String> {
    AlertDialog alertDialog;
    Context ctx;
    ForgotPassword(Context ctx)
    {
        this.ctx = ctx;

    }


    @Override
    protected void onPreExecute() {

        alertDialog = new AlertDialog.Builder(ctx).create();
        alertDialog.setTitle("Login Information...");

    }

    @Override
    protected String doInBackground(String... params) {

        String reg_url = "http://192.168.2.121/Webdev/forgot_password.php";

        String method = params[0];
        if(method.equals("forgotpassword"))
        {
            String employee_id = params[1];
            String email = params[2];
            try {
                URL url = new URL(reg_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("employee_id","UTF-8")+"="+URLEncoder.encode(employee_id,"UTF-8")+"&"+
                        URLEncoder.encode("email","UTF-8")+"="+ URLEncoder.encode(email, "UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                OS.close();
                InputStream IS  = httpURLConnection.getInputStream();
                IS.close();
                return  "ok";
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }



        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {

        alertDialog.setMessage("Please check your email");



    }
}
